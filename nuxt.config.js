import Api from "./services/Api";
require("dotenv").config();

const isProduction = process.env.NODE_ENV === "production";
const isTrackingEnabled = process.env.TRACKING === "true" ? true : false;

export default {
  target: "static", // default is 'server'
  //mode: 'universal',
  server: {
    port: 8080,
    host: "localhost"
  },
  env: {
    appsEducationUrl: process.env.APPS_EDUCATION_URL
      ? process.env.APPS_EDUCATION_URL
      : "https://portail.apps.education.fr",
    tchapChannelUrl: process.env.TCHAP_CHANNEL_URL
      ? process.env.TCHAP_CHANNEL_URL
      : "https://www.tchap.gouv.fr/#/room/#apps.education.fr:agent.education.tchap.gouv.fr",
    keyStatEnabled: true,
    isTrackingEnabled
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "apps.education.fr",
    htmlAttrs: {
      lang: "fr"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600;700&display=swap"
      }
    ],
    // Tracking Matomo si on est en production
    script: isProduction && isTrackingEnabled ? [{ src: "/js/matomo.js" }] : []
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#212F74" },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ["~/plugins/i18n.js", "~/plugins/global.js"],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    ["nuxt-buefy", { css: false }],
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/style-resources"
  ],
  styleResources: {
    scss: ["./assets/styles/_settings.scss"]
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  /*
   ** Static files
   */
   generate: {
    dir: "public",
    async routes() {
      let routes = [];
      const applications = await Api.findAllApplications();
      routes = routes.concat(
        applications.map(application => "/applications/" + application.id)
      );
      const apps_externes = await Api.findAllApplications_externes();
      routes = routes.concat(
        apps_externes.map(app_externe => "/apps_externes/" + app_externe.id)
      );
  
      return routes;
    }
  }
  
};
