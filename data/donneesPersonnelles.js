export default `
<h1>Protection et traitement de données à caractère personnel </h1>

	<h2>Sommaire</h2>
	  <ul>
		<li><a href="#apps">Données personnelles - Site apps.education.fr et plateforme de présentation de services</a></li>
		<li><a href="#sso">Données personnelles - Service de SSO</a></li>
		<li><a href="#forums">Données personnelles - Forums</a></li>
		<li><a href="#cloud">Données personnelles - Nextcloud</a></li>
		<li><a href="#pads">Données personnelles - Pads</a></li>
	  	<li><a href="#peertube">Données personnelles - Peertube</a></li>
	  	<li><a href="#codiMD">Données personnelles - CodiMD</a></li>
	  	<li><a href="#forge">Données personnelles - Forge des communs numériques éducatifs</a></li>
	  </ul>

	<h2 id="apps"><strong>Données personnelles - Site apps.education.fr et plateforme de présentation de services</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>
 
	<h2 id="sso"><strong>Données personnelles - SSO</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>




<h2 id="forums"><strong>Données personnelles - Forums</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>
<h2 id="cloud"><strong>Données personnelles - Nextcloud</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>

<h2 id="pads"><strong>Données personnelles - Pads</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>


<h2 id="peertube"><strong>Données personnelles - Peertube</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>


<h2 id="codiMD"><strong>Données personnelles - CodiMD</strong></h2>

		<p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>

		<p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
			<ul>
				<li>les données de fréquentation : 12 mois </li>
				<li>les données techniques : journalisation des connexions 12 mois glissants</li>
				<li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
				<li>données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
			</ul>
		</p>

		<p>Les catégories de destinataires des données à caractère personnel sont :
			<ul>
				<li>les administrateurs système habilités ;</li>
				<li>les administrateurs fonctionnels habilités ;</li>
				<li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
				<li>chacun des utilisateurs identifiés pour se propres données ;</li>
				<li>les autres utilisateurs  identifiés (lorsque les données sont restreintes à ce public) ;</li>
				<li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
			</ul>
		</p>

		<p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie  dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante :
		<a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>

		<p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
		<p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :
			<ul>
				<li>à l’adresse électronique suivante : dpd@education.gouv.fr</li>
				<li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
				<li>ou par courrier en vous adressant au : Ministère de l'éducation nationale et de la jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
			</ul>
		</p>
		<p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

		<h3>Données statistiques, traçabilité</h3>
			<p>Lors de leur navigation sur ces deux sites, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. </p>

			<p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO. </p>
		<h3>Cookies</h3>

		<p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>

		<p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>

		<p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
		<p>Les données collectées de façon anonyme sont conservées 12 mois.</p>

		<p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>

		<p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :
			<ul>
				<li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
				<li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. </li>
			</ul>
		</p>

		<p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. </p>

		<p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

		<h3>Journalisation </h3>

		<p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>

	<h2 id="forge"><strong>Données personnelles - Forge des communs numériques éducatifs</strong></h2>
    <p>Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’Éducation nationale et de la Jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
    <p>Le ministère de l’Éducation nationale et de la Jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
    <p>Les catégories de données à caractère personnel concernées et leurs durées de conservation sont :</p>
    <ul>
        <li>les données de fréquentation : 12 mois</li>
        <li>les données techniques : journalisation des connexions 12 mois glissants</li>
        <li>les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.</li>
        <li>les données produites par les utilisateurs identifiés ou sous leurs responsabilités</li>
    </ul>
    <p>Les catégories de destinataires des données à caractère personnel sont :</p>
    <ul>
        <li>les administrateurs système habilités ;</li>
        <li>les administrateurs fonctionnels habilités ;</li>
        <li>les administrateurs du pôle de supervision (analyse statistique) ;</li>
        <li>chacun des utilisateurs identifiés pour se propres données ;</li>
        <li>les autres utilisateurs identifiés (lorsque les données sont restreintes à ce public) ;</li>
        <li>le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.</li>
    </ul>
    <p>Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, d’information, de rectification, d’effacement, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante : <a href="https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557">https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557</a></p>
    <p>De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.</p>
    <p>Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse :</p>
    <ul>
        <li>à l’adresse électronique suivante : <a href="mailto:dpd@education.gouv.fr">dpd@education.gouv.fr</a></li>
        <li>via le formulaire de saisine en ligne : <a href="http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD">http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD</a></li>
        <li>ou par courrier en vous adressant au : Ministère de l'Éducation nationale et de la Jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07</li>
    </ul>
    <p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).</p>

    <h3 id="donnees-statistiques-tracabilite">Données statistiques, traçabilité</h3>
    <p>Lors de leur navigation sur le site, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie.</p>
    <p>Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO.</p>

    <h3 id="cookies">Cookies</h3>
    <p>Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.</p>
    <p>Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.</p>
    <p>Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.</p>
    <p>Les données collectées de façon anonyme sont conservées 12 mois.</p>
    <p>La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.</p>
    <p>Les cookies sont déposés sur l’équipement de l'internaute. Ce sont :</p>
    <ul>
        <li>les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;</li>
        <li>les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours.</li>
    </ul>
    <p>Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services.</p>
    <p>Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur</p>

    <h3 id="journalisation">Journalisation</h3>
    <p>Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.</p>

`;
