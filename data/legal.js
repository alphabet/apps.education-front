export default `
<h1>Mentions légales</h1>

<h2>Sommaire</h2>
  <ul>
	<li><a href="#apps">Mentions légales - Site apps.education.fr et plateforme de présentation de services</a></li>
	<li><a href="#sso">Mentions légales - Service de SSO</a></li>
	<li><a href="#forums">Mentions légales - Forums</a></li>
	<li><a href="#cloud">Mentions légales - Nuage</a></li>
  	<li><a href="#tube">Mentions légales - Tube</a></li>
  	<li><a href="#codiMD">Mentions légales - CodiMD</a></li>
	<li><a href="#forge">Mentions légales - Forge des communs numériques éducatifs</a></li>
  </ul>



<h2 id="apps"><strong>Mentions légales - Site apps.education.fr et plateforme de présentation de services</strong></h2>

<h3>Présentation</h3>
	<p>Ce site web <a href="https://projet.apps.education.fr/"> projet.apps.education.fr</a> ainsi que la plateforme de présentation des services <a href="https://portail.apps.education.fr/" target="_blank"> portail.apps.education.fr</a> proposent un ensemble de services de communication et de partage de documents transmedia réservés aux personnels de l'éducation nationale.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Hébergement : </h3>
		<p>
			<ul>
				<li>Le site <a href="https://projet.apps.education.fr/"> projet.apps.education.fr</a> est hébergé par le ministère de l'Éducation nationale sur les infrastructures mutualisées PHM à Osny (95).</li>
				<li>La plateforme de présentation des services applicatifs est hébergée par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</li>
			</ul>
<p>Ces services constituent un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#apps">Protection et traitement des données à caractère personnel</a>.</p>



<h2 id="sso"><strong>Mentions légales - Service de SSO</strong></h2>


<h3>Présentation</h3>
	<p>Ce service permet à l’utilisateur identifié d'accéder à un ensemble de services de communication et de partage de documents transmedia réservés en écriture aux personnels de l'éducation nationale.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Benoît Piédallu</li>
				<li>Coordination technique  : Nicolas Schont</li>
				<li>Coordination des développements : Pôle de compétences Logiciels Libres</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Via le forum : https://forum.eole.education</p>



	<h3>Hébergement : </h3>
		<p>Le service de SSO est hébergé par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</p>

		<p>Ce service de SSO constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#sso">Protection et traitement des données à caractère personnel</a>.</p>


<h2 id="forums"><strong>Mentions légales - Forums</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service accessible à l'adresse <a href="https://forum.eole.education/" target="_blank"> forum.eole.education</a> permet à l’utilisateur identifié d'accéder à un service de forum permettant la communication et le partage de documents transmedia. Le forum est réservés en écriture aux personnels de l'éducation nationale. L’accès en lecture au service de forum est public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Benoît Piédallu</li>
				<li>Coordination technique  : Nicolas Schont</li>
				<li>Coordination des développements : Pôle de compétences Logiciels Libres</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : Assistance de votre académie</p>



	<h3>Hébergement : </h3>
		<p>Le service de Forums est hébergé par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</p>

		<p>Ce service de Forums constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#forums">Protection et traitement des données à caractère personnel</a>.</p>


<h2 id="cloud"><strong>Mentions légales - Nuage</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de publication de fichiers est accessible à l’adresse <a href="https://nuage.apps.education.fr/" target="_blank"> nuage.apps.education.fr</a>  et il permet à l’utilisateur identifié d'accéder à un service de stockage et de publication de fichiers permettant la communication et le partage de documents transmedia. Le service de stockage et de publication de fichiers est accessible en écriture aux personnels de l'éducation nationale. Les élèves peuvent être invités à déposer ou à modifier des documents. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Benoît Piédallu</li>
				<li>Coordination technique  : Nicolas Schont</li>
				<li>Coordination des développements : DSI de Versailles</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
	<p>Assistance de niveau 1 aux utilisateurs : Assistance de votre académie</p>

	<h3>Hébergement : </h3>
		<p>Le service Nuage est hébergé au sein des hébergements nationaux du MENJS</p>

		<p>Ce service d’hébergement de fichiers constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#cloud">Protection et traitement des données à caractère personnel</a>.</p>



<h2 id="tube"><strong>Mentions légales - Tube</strong></h2>

	<h3>Présentation</h3>
		<p>Ce service de publication de vidéogrammes est accessible à l’adresse tubes.apps.education.fr. Il permet à l’utilisateur identifié d'accéder à service de stockage et de diffusion de vidéogrammes. Le service de publication de vidéogrammes est accessible en écriture aux personnels de l'éducation nationale. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Benoît Piédallu</li>
				<li>Coordination technique  : Nicolas Schont</li>
				<li>Coordination des développements : Pôle de compétences Logiciels Libres</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
	<p>Assistance de niveau 1 aux utilisateurs : Assistance de votre académie</p>



	<h3>Hébergement : </h3>
		<p>Le service Tube est hébergé par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</p>

		<p>Ce service de publication de vidéogrammes constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#peertube">Protection et traitement des données à caractère personnel</a>.</p>

<h2 id="codiMD"><strong>Mentions légales - CodiMD</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de création et d’édition de textes est accessible à l’adresse <a href="https://codimd.apps.education.fr/" target="_blank"> codimd.apps.education.fr</a>. Il permet à l’utilisateur identifié d'accéder à un éditeur de texte et à un stockage de textes permettant la communication et le partage de documents transmedia. Le service d’édition de texte est accessible en écriture aux personnels de l'éducation nationale. Les élèves peuvent être invités à modifier des textes sous la responsabilité des enseignants. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Audran Le Baron,
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Benoît Piédallu</li>
				<li>Coordination technique  : Nicolas Schont</li>
				<li>Coordination des développements : Pôle de compétences Logiciels Libres</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
	<p>Assistance de niveau 1 aux utilisateurs : Assistance de votre académie</p>


	<h3>Hébergement : </h3>
		<p>Le service CodiMD est hébergé par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</p>

		<p>Ce service d’hébergement de fichiers constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://projet.apps.education.fr/donnees-personnelles/#codiMD">Protection et traitement des données à caractère personnel</a>.</p>

<h2 id="forge"><strong>Mentions légales - Forge des communs numériques éducatifs</strong></h2>
    <h3 id="presentation">Présentation</h3>
    <p>Ce service, appelé Forge des communs numériques éducatifs, permet à l’utilisateur identifié d'accéder à une forge logicielle, reposant sur le logiciels libre GitLab Community Edition, pour y déposer, collaborer et partager du code et du contenu à vocation pédagogique. Il est réservé en écriture aux personnels de l'éducation nationale et aux partenaires inscrits pour une durée limitée et renouvelable sur un annuaire externe des comptes invités. L’accès en lecture aux ressources de la Forge des communs numériques éducatifs peut être public.</p>

    <h3 id="identification-de-lediteur">Identification de l'éditeur</h3>
    <p>Ministère de l'Éducation nationale et de la Jeunesse<br>
    Direction du numérique pour l'éducation<br>
    110 rue Grenelle, 75007 Paris</p>

    <h3 id="directeur-de-publication">Directeur de publication</h3>
    <p>M. Audran Le Baron, Directeur du numérique pour l’éducation</p>

    <h3 id="maitrise-douvrage-et-redaction">Maîtrise d'ouvrage et rédaction</h3>
    <p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
    <ul>
        <li>Responsable de production : Laurent Le Prieur</li>
        <li>Chef de projet : Benoît Piédallu</li>
        <li>Porteur fonctionnel : Alexis Kauffmann</li>
        <li>Coordination des développements : Pôle de compétences Logiciels Libres</li>
    </ul>

    <h3 id="accompagnement-et-support">Accompagnement et support</h3>
    <p>Assistance de niveau 1 aux utilisateurs : Assistance de votre académie</p>
    <p>Assistance de niveau 2 et modération : DRANE site de Lyon</p>
    <p>Assistance technique de niveau 3 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
    <ul>
        <li>mail de contact : <a href="mailto:forge-commun-numeriques@ldif.education.gouv.fr">forge-commun-numeriques@ldif.education.gouv.fr</a></li>
        <li>forum Tchap : <a href="https://www.tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK">https://www.tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK</a></li>
    </ul>

    <h3 id="hebergement">Hébergement</h3>
    <p>Le service de la Forge des communs numériques éducatifs est hébergé par AVENIR TELEMATIQUE situé 21 avenue de la Créativité, 59650 Villeneuve d’Ascq en France.</p>
    <p>Le service de la Forge des communs numériques éducatifs fait l’objet d’un traitement de données à caractère personnel mis en œuvre par le ministre de l’Éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
    <p>Le ministère de l’Éducation nationale et de la Jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
    <p>Les indications précises relatives au traitement des données à caractère personnel sont disponibles dans le document <a href="#">Protection et traitement des données à caractère personnel</a>.</p>

`;
