
export {default as applications} from './applications';
export {default as platform} from './platform';
export {default as cgu} from './cgu';
export {default as accessibilite} from './accessibilite';
export {default as legal} from './legal';
export{default as apps_externes} from './apps_externes';
export {default as donneesPersonnelles} from './donneesPersonnelles';