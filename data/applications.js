export default [
  {
    id: "peertube",
    title: "Peertube",
    subtitle: "Vidéos",
    description: "Publication et partage de vidéos",
    keyStat: "300 vidéos publiées par mois",
    icon: "peertube",
    texts: [
      {
        title: "Partagez vos vidéos avec Peertube",
        body: `<p>PeerTube est un service mis à disposition par le Ministère de l'Education Nationale et de la Jeunesse.</p>
          <p>Il permet de déposer, d'héberger et de consulter des média audios et vidéos. Il répond aux conditions préconisées dans le cadre du règlement général de la protection des données (RGPD).</p>
          <p>Ce service, à destination de l'ensemble des personnels de l'éducation nationale, permet&nbsp;:</p>
          <ul>
            <li>sans authentification de <strong>consulter</strong> les médias mis à disposition</li>
            <li>ou après authentification par l'adresse professionnelle, de <strong>déposer</strong>, <strong>partager</strong> ou <strong>télécharger des médias</strong>, dans le cadre de la charte d'utilisation. L'auteur reste responsable du contenu de ses productions et doit s’assurer qu’il dispose bien des droits de publication nécessaires.</li>
          </ul>`
      }
    ]
  },
  {
    id: "classe-virtuelle",
    title: "Classe Virtuelle",
    subtitle: "Visio-conférence",
    description: "",
    keyStat: "",
    icon: "webconference",
    texts: [
      {
        title: "Visio-conférence entre professeurs et élèves",
        body: `<p>Service de classe virtuelle mis à disposition par le Ministère de l’Éducation Nationale et de la Jeunesse.</p>
        <p>Il permet la continuité pédagogique dans le cas d'une fermeture de classe, partielle ou complète, en proposant un environnement de travail complet : vidéo, tableau blanc, votes, partage de documents, etc. Il répond aux conditions préconisées dans le cadre du règlement général sur la protection des données (RGPD).</p>
        <p>Ce service, à destination des enseignants de l'éducation nationale et de leurs élèves, permet :</p>
        <ul>
          <li>d'organiser plusieurs classes virtuelles avec des paramètres différents</li> 
          <li>d'inviter des élèves, authentifiés via educonnect ou non, jusqu'à 350 si seule la caméra de l'enseignant est activée</li>
        </ul>
        <p>Les options permettent au besoin de limiter l'usage de la messagerie instantanée ou de la rédaction collaborative. L'enregistrement des sessions n'est pas autorisé pour cause de droits à l'image.</p>
        `
      }
    ]
  },

  {
    id: "nextcloud",
    title: "Nextcloud",
    subtitle: "Partage de documents",
    description: "Service d'hébergement, gestion et partage de fichiers.",
    keyStat: "10000 fichiers partagés",
    icon: "nextcloud",
    texts: [
      {
        title: "Partager des fichiers avec Nextcloud",
        body: `
          <p>Nextcloud est un service <a href="https://primabord.eduscol.education.fr/qu-est-ce-que-l-informatique-dans-les-nuages-ou-le-cloud-computing" target="_blank">cloud</a> proposé par le Ministère de l'Education Nationale et de la Jeunesse.</p>
          <p>Il met à disposition un espace de stockage en ligne sécurisé pour diffuser et partager des documents et des ressources téléchargeables à partir d'un poste de travail ou d'un appareil mobile.</p>
          <p>Accessible après authentification par son adresse professionnelle, ce service facilite le travail collaboratif entre les personnels de l'Éducation nationale. Il répond aux conditions préconisées dans le cadre du règlement général de la protection des données (RGPD).</p>
          <p>Ce service permet&nbsp;:</p>
          <ul>
            <li>de stocker et de synchroniser les documents afin de les maintenir à jour en temps réel&nbsp;;</li>
            <li>d'enregistrer et d'accéder aux données à tout moment et en tout lieu&nbsp;;</li>
            <li>de travailler en toute sécurité en garantissant la confidentialité des données.&nbsp;;</li>
          </ul>
          <p>Chaque utilisateur disposera d'un espace de stockage d'une taille de 100 Go sauf réglage spécifique décidé localement.</p>
          `
      }
    ]
  },
  {
    id: "blogs",
    title: "Blogs",
    subtitle: "Publication sur le web",
    description: "Service d'édition et de publication de blogs.",
    keyStat: "200 blogs actifs",
    icon: "blogs",
    texts: [
      {
        title: "Publiez vos contenus avec le service de blogs",
        body: `
          <p>Un blog est un espace sur internet où l’on partage une expérience et où l'on donne un point de vue personnel (et professionnel dans notre contexte ) sur un sujet donné.</p>
          <p>Ce blog, à destination des personnels de l'Éducation nationale offre, après authentification, la possibilité de communiquer et de prolonger le travail collectif d'une classe ou d'un groupe de professionnels. Ce service sécurisé permet notamment de partager, selon une organisation définie par son auteur, des textes, des images, des vidéos ...</p>
          <p>Pour mémoire, l'utilisation d'un blog doit respecter une charte d'utilisation et des mentions légales.</p>
          <p>Ce service permet notamment&nbsp;:</p>
          <ul>
            <li>de former les élèves  à un usage éclairé et responsable du numérique&nbsp;;</li>
            <li>de sensibiliser les élèves à la protection des données personnelles, à la propriété intellectuelle et au droit à l’image&nbsp;;</li>
            <li>de développer l'esprit critique&nbsp;;</li>
            <li>de documenter les productions et les projets de classe.&nbsp;;</li>
          </ul>
          <p>Ce service est un outil de communication au sein de la communauté éducative pour&nbsp;:</p>
          <ul>
            <li>les élèves dans et en dehors du temps et de l'espace scolaires, il participe à la continuité pédagogique&nbsp;;</li>
            <li>les parents en lien avec la coéducation&nbsp;;</li>
            <li>les professionnels de l'éducation dans le cadre de leurs différentes missions.</li>
          </ul>
          `
      }
    ]
  },
  {
    id: "codi-md",
    title: "CodiMD",
    subtitle: "Rédaction à plusieurs",
    description: "Service de rédaction collaborative.",
    keyStat: "100 articles rédigés",
    icon: "etherpad",
    texts: [
      {
        title: "La rédaction collaborative avec CodiMD",
        body: `<p>CodiMD est un éditeur collaboratif de notes et diaporama, écrits en Markdown.<p><p>Il s'inspire d'Etherpad et d'autres éditeurs collaboratifs similaires..<p>`
      }
    ]
  },
  {
    id: "pod",
    title: "Pod",
    subtitle: "Vidéos & audios",
    description: "Service de rédaction collaborative.",
    keyStat: "100 vidéos publiées",
    icon: "peertube",
    texts: [
      {
        title: "La diffusion de médias audios et vidéos avec Pod",
        body: `<p>
        Pod est une plate-forme académique de dépôt et de diffusion de documents vidéo et audio. Elle permet de diffuser en toute sécurité et sans publicité vos vidéos, en particulier à destination de vos élèves, pour des capsules en classe inversée, des ressources pédagogiques, des tutoriels mais aussi pour des conférences, la présentation d'une filière ou de gestes métiers... 
        <p>`
      }
    ]
  },
  {
    id: "visio_agents",
    title: "Visio-Agents",
    subtitle: "Visio-conférence entre agents",
    description: "",
    keyStat: "",
    icon: "visio_agents",
    texts: [
      {
        title: "Service de vidéoconférence et réunions de travail à distance du ministère de l’Éducation nationale",
        body: `<p>
        Service de visioconférence pour des réunions entre agents, Webinaire permet à plusieurs dizaines de personnes -jusqu’à 550, déjà testé avec les caméras non activées- de faire des réunions en visioconférence. 
        <p>Tout utilisateur peut créer un ou plusieurs salons depuis son interface de gestion, laquelle lui donne ensuite la possibilité de donner accès à ce salon à 2 type de participants : les modérateurs et les participants.</p>
        <p> Il lui est aussi offert la possibilité de personnaliser sa salle avec titre et texte d’accueil. Dans le salon, il est possible à un présentateur de présenter et de partager des documents et des vidéos. Tous les participants peuvent discuter ensemble par écrit dans un outil de chat, et prendre des notes partagées.</p>
        <p> Il est aussi possible pour les modérateurs de créer des sondages parmi les participants, dont les résultats seront partagés pendant la session. Si l’organisateur l’a prévu, la session pourra être enregistrée et mise à disposition ultérieurement au téléchargement. Dans un cadre pédagogique, il peut être intéressant de profiter de la fonction de sous-salon pour diviser les participants en sous-groupes.
        <p>`
      }
    ]
  }
  

  /* pas d'atelier pour le moment
   {
    "id": "latelier",
    "title": "L'Atelier",
    "subtitle": "Gestion de projets",
    "description": "",
    "keyStat": "13000 tâches réalisées",
    "icon": "nextcloud",
    "texts":[
        {
          "title":"Gérer vos projets grâce à l'Atelier",
          "body":
            `<p>L'Atelier est un outil de gestion de projets. Il met à disposition pour les personnels de l'Éducation nationale des outils pour s'organiser et mener à bien vos missions</p>
            <p>Ce service permet notamment :</p>
            <ul>
              <li>La gestion et la création des tâches de chaque projet grâce aux tableaux Kanban, aux calendriers de projet</li>
              <li>L'organisation de réunions, avec un calendrier et la rédaction de comptes-rendus de réunions</li>
              <li>La description de processus métiers via la norme BPMN</li>
              <li>La gestion des différents projets, organisations, et de leurs utilisateurs et permissions</li>
            </ul>
            `
          }
        ]
    }, */
];
