export default `
  <h1>Conditions générales d'utilisation</h1>
    

  <h2>Sommaire</h2>
  <ul>
	<li><a href="#apps">Conditions générales d&#39;utilisation - Site apps.education.fr et plateforme de présentation de services</a></li>
	<li><a href="#sso">Conditions générales d&#39;utilisation - Service de SSO</a></li>
	<li><a href="#forums">Conditions générales d&#39;utilisation - Forums</a></li>
	<li><a href="#cloud">Conditions générales d&#39;utilisation - Nuage</a></li>
	<li><a href="#pads">Conditions générales d&#39;utilisation - Pads</a></li>
  	<li><a href="#peertube">Conditions générales d&#39;utilisation - Peertube</a></li>
  	<li><a href="#CodiMD">Conditions générales d&#39;utilisation - CodiMD</a></li>
  	<li><a href="#forge">Conditions générales d&#39;utilisation - Forge des communs numériques éducatifs</a></li>
  	<li><a href="#questionnaire">Conditions générales d&#39;utilisation - Outil de création de formulaire en ligne (Questionnaire)</a></li>
  </ul>

	<h2 id="apps"><strong>Conditions générales d&#39;utilisation - Site apps.education.fr et plateforme de présentation de services</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le site apps.education.fr et la plateforme de présentation des services apps-(nom de l’académie).beta.education.fr ou à toute information publiée sur le site, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du site et des contenus qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du Site web apps.education.fr et la plateforme de présentation des services apps-(nom de l’académie).beta.education.fr.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du Site web apps.education.fr et la plateforme de présentation des services apps-(nom de l’académie).beta.education.fr.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https://apps.education.fr et https://apps-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui visite une ou plusieurs pages du Site apps.education.fr et apps-(nom de l’académie).beta.education.fr. </p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>apps.education.fr et apps-(nom de l’académie).beta.education.fr est un site web d’information et d’accès aux services accessibles par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le Site apps.education.fr donne accès aux informations sur le projet de service numérique partagés (SNP).</p>
			<p>Le Site apps-(nom de l’académie).beta.education.fr présente les informations relatives aux services accessibles par académie en mode identifié ainsi qu’au dispositif de création de comptes utilisateurs réservés aux enseignants.</p>
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Site.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Site, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux Site apps.education.fr et au Site apps-(nom de l’académie).beta.education.fr est réservé aux personnes travaillant pour l’éducation nationale qui disposent d’un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et aux élèves régulièrement inscrits auprès d’une école ou d’un établissement scolaire.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Site sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du site, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://apps-(mon académie).beta.education.fr ou sur les sous-domaines associés, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Site n’entraîne aucune cession des droits susvisés.
			Les droits d’utilisation du site ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.
			L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Site, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>


	<h2 id="sso"><strong>Conditions générales d&#39;utilisation - Service de SSO</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, Utilisateur identifié pour accéder au service de SSO à l’adresse https://sso-(nom de l’académie).beta.education.fr, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service de SSO et des services associés qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service de SSO.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du service de SSO.</p>
			<p><u>Site</u> : désigne le site ou le service de SSO accessible depuis l’adresse https://sso-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui utilise le service de SSO, service dont l’accès est réservé à l’Utilisateur sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités des autres services. </p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>Le SSO est un service d’authentification unique accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service de SSO offre des fonctionnalités suivantes : 
				<ul>
					<li>il permet de créer un compte à chaque utilisateur ayant droit, c’est à dire disposant d’une adresse académique de la forme utilisateur@ac-(nom de l’académie).fr ;</li>
					<li>il permet ensuite de passer un jeton à tous les services créés par le Ministère en charge de l’éducation nationale dans le cadre de la continuité pédagogique liée au COVID-19.</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du service de SSO est réservé aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié).</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>A l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service de SSO sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du site, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://sso-(nom de l’académie).beta.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.
			Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>



	<h2 id="forums"><strong>Conditions générales d&#39;utilisation - Service de Forums</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le service de Forum ou à toute information publiée sur le service Forum, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service de Forum et des contenus associés qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service de SSO.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du Service de Forum.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https:/forum-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui écrit sur une ou plusieurs pages du Service de Forum, espace dont l’accès en écriture est réservé à l’Utilisateur identifié, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités.</p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>Le Forum est un service de publication structurée par sujets accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service de Forum offre des fonctionnalités suivantes :  
				<ul>
					<li>publication de textes accompagnés de pièces jointes et d’image ;</li>
					<li>commentaires et réponses au textes ;</li>
					<li>organisation par fils ;</li>
					<li>recherche dans les sujets et dans les textes.</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès sur le Service de Forum est réservé en écriture aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en lecture seule aux élèves régulièrement inscrits auprès d'une école ou d’un établissement scolaire. Le service est également accessible en lecture au grand public.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>A l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service de Forum sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://forum-(nom de l’académie).beta.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.
			Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>



	<h2 id="cloud"><strong>Conditions générales d&#39;utilisation - Service de Cloud</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le Service de Publication de Fichier (Cloud) ou à toute information publiée sur le Service, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service et des contenus qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du site ou du service.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https:/nuage-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui écrit sur une ou plusieurs pages du Service, espace dont l’accès est réservé à l’Utilisateur identifié, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités.</p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>Nuage est un service de publication et de partage de fichiers accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service Cloud offre des fonctionnalités suivantes :  
				<ul>
					<li>dépôt de fichiers unitaire ;</li>
					<li>organisation des espaces de stockage par création d’arborescence de dossiers ;</li>
					<li>publication d'un fichier ou d'un dossier.</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du service est réservé aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en lecture seule aux élèves régulièrement inscrits auprès d'une école ou d’un établissement scolaire.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service Nuage sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://nuage**.apps.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.</p>
			<p>Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents sur le Service, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser lui même la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>


	<h2 id="pads"><strong>Conditions générales d&#39;utilisation - Service d'édition de texte simple ("Pads")</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le Service d’éditeur de texte ou à toute information publiée sur le Service, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service et des contenus qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du site ou du service.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https:/pad-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui écrit sur une ou plusieurs pages du Service, espace dont l’accès est réservé à l’Utilisateur identifié, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités.</p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>Pad est un service de traitement de texte simple accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service Pad offre des fonctionnalités suivantes :  
				<ul>
					<li>édition simplifiée de textes ;</li>
					<li>publication de textes en mode public (avec ou sans mot de passe) ;</li>
					<li>édition publique (avec ou sans mot de passe) ;</li>
					<li>organiser les documents en dossier et sous dossier ;</li>
					<li>partager des documents avec d'auters utilisateurs à partir de leur adresse de messagerie</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du service est réservé aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en lecture seule aux élèves régulièrement inscrits auprès d'une école ou d’un établissement scolaire.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service de Pads sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://(nom du service)-(nom de l’académie).beta.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.</p>
			<p>Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents sur le Service, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser lui même la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne ou modérés par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>


	<h2 id="peertube"><strong>Conditions générales d&#39;utilisation - Peertube</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le Service d’éditeur de texte ou à toute information publiée sur le Service, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service et des contenus qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du site ou du service.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https:/tube-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui visite une ou plusieurs pages du Service de publication de vidéogrammes, espace dont l’accès pour des dépôts est réservé à l’Utilisateur sur le Site, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités. </p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>Peertube est un service de publication de vidéogrammes accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service Peertube offre des fonctionnalités suivantes :  
				<ul>
					<li>dépôt de vidéogrammes avec ses données d’indexation réservée aux utilisateurs identifiés ;</li>
					<li>diffusion publique des vidéogrammes (sans contrôle d’accès).</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du Service Peertube est réservé en écriture (dépôt de vidéogrammes) aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en lecture au grand public et donc aux élèves régulièrement inscrits auprès d'une école ou d’un établissement scolaire.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service Peertube sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://(nom du service)-(nom de l’académie).beta.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.</p>
			<p>Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents sur le Service, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser lui même la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne ou modérés par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>

			<p><strong>Complément documentaire : </strong></p>
			<p><u>Documents pour obtenir les droits à l'image</u></p>
			<p>De nombreux documents relatif au respect du droit à l’image sont disponibles sur le site "Internet Responsable" (<a href="https://eduscol.education.fr/cid149770/protection-des-donnees-personnelles.html" target="_blank">https://eduscol.education.fr/cid149770/protection-des-donnees-personnelles.html</a>)





	<h2 id="CodiMD"><strong>Conditions générales d&#39;utilisation - CodiMD</strong></h2>
		<h3><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser le Service d’éditeur de texte ou à toute information publiée sur le Service, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service et des contenus qui sont disponibles.</p>
		<h3><strong>2. Définitions</strong> </h3>
			<p><u>Condition Générale d’Utilisation (« CGU »)</u> : désigne les présentes conditions d’utilisations du service.</p>
			<p><u>Éducation nationale</u> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><u>Service</u> : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du site ou du service.</p>
			<p><u>Site</u> : désigne le site accessible depuis l’adresse https:/codi-(nom de l’académie).beta.education.fr.</p>
			<p><u>Utilisateur identifié</u> : désigne toute personne physique qui écrit sur une ou plusieurs pages du Service, espace dont l’accès est réservé à l’Utilisateur identifié, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités.</p>

		<h3><strong>3. Présentation du site</strong></h3>
			<p>CodiMD est un service de traitement de texte avancé accessible par navigateurs et terminaux mobiles pour la communauté éducative pendant la période de confinement liée au COVID-19.</p>
			<p>Le service Pad offre des fonctionnalités suivantes :  
				<ul>
					<li>édition de textes en markdown;</li>
					<li>publication de textes en mode public (avec ou sans mot de passe) ;</li>
					<li>édition publique (avec ou sans mot de passe) ;</li>
					<li>partager des documents avec d'autres utilisateurs.</li>
				</ul>
			</p>
		
		<h3><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>
		<h3><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du service est réservé aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en lecture seule aux élèves régulièrement inscrits auprès d'une école ou d’un établissement scolaire.</p>
			<p>L’Utilisateur s’engage à ne pas :
				<ul>
					<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
					<li>Utiliser le site : 
						<ul>
							<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
							<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
							<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
							<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
							<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; </li>
							<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
							<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
							<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
						</ul>
					</li>
				</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>
		<h3><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service CodiMD sont protégés par la législation française sur le droit d'auteur et le droit des marques. </p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse https://(nom du service)-(nom de l’académie).beta.education.fr, sont la propriété exclusive du Ministère en charge de l’éducation nationale.</p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.</p>
			<p>Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). </p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents sur le Service, il est lui même considéré comme responsable des publications réalisées sur le service et doit organiser lui même la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>
		<h3><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>
		<h3><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne ou modérés par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
		<h3><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 21 avril 2020.</p>
		<h3><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>
		<h3><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>


	<h2 id="forge"><strong>Conditions générales d&#39;utilisation - Forge des communs numériques éducatifs</strong></h2>

		<h3 id="objet"><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, Utilisateur identifié pour accéder au service de la Forge des communs numériques éducatifs à l’adresse forge.apps.education.fr/, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service de SSO et des services associés qui sont disponibles.</p>

		<h3 id="definitions"><strong>2. Définitions</strong></h3>
			<p><strong>Condition Générale d’Utilisation (« CGU »)</strong> : désigne les présentes conditions d’utilisations du service de la Forge des communs numériques éducatifs.</p>
			<p><strong>Éducation nationale</strong> : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p><strong>Service</strong> : désigne le service fourni par Ministère en charge de l’Éducation nationale et ses sous-traitants, accessible par le biais du service de la Forge des communs numériques éducatifs.</p>
			<p><strong>Site</strong> : désigne le site ou le service de la Forges des communs numériques éducatifs accessible depuis l’adresse <a href="https://forge.apps.education.fr/">https://forge.apps.education.fr/</a>.</p>
			<p><strong>Utilisateur identifié</strong> : désigne toute personne physique qui utilise le service de la Forges des communs numériques éducatifs, service dont l’accès est réservé à l’Utilisateur sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités des autres services.</p>

		<h3 id="presentation-du-site"><strong>3. Présentation du site</strong></h3>
			<p>Ce service permet à l’utilisateur identifié d’accéder à une forge logicielle, reposant sur le logiciels libre GitLab Community Edition, pour y déposer, collaborer et partager du code et du contenu à vocation pédagogique. Il est réservé en écriture aux personnels de l’Éducation nationale et aux partenaires inscrits pour une durée limitée et renouvelable sur un annuaire externe des comptes invités. L’accès en lecture aux ressources de la Forge des communs numériques éducatifs peut être public.</p>

		<h3 id="conditions-d-acces-generales"><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’Éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’Éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’Éducation nationale ne peut, en outre, être tenu responsable de tout traitement des données personnelles ne relevant pas des dispositions e) du 1 de l’article 6 du RGPD publié sur la Forge des communs numériques éducatifs.</p>
			<p>Le Ministère en charge de l’Éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>

		<h3 id="regles-d-utilisation-du-service-et-engagement-de-l-utilisateur"><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès aux fonctionnalités du service de la Forge des communs numériques éducatifs est réservé aux personnes travaillant pour l’Éducation nationale et aux comptes invités par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié).</p>
			<p>L’Utilisateur s’engage à ne pas :</p>
			<ul>
				<li>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</li>
				<li>Utiliser le site :
					<ul>
						<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
						<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
						<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
						<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
						<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ;</li>
						<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
						<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’Éducation nationale, ou à tout autre utilisateur du Service ;</li>
						<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
					</ul>
				</li>
			</ul>
			<p>Le Ministère en charge de l’Éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>

		<h3 id="propriete-intellectuelle"><strong>6. Propriété intellectuelle</strong></h3>
			<p>A l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service de la Forge des communs numériques éducatifs sont, sauf mention contraire, sous <a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence/">Licence Ouverte 2.0</a>.</p>
			<p>Il appartient à l’Utilisateur identifié de choisir lui-même la licence des contenus qu'il publie sur la Forge des communs numériques éducatifs <a href="https://www.data.gouv.fr/fr/pages/legal/licences/">parmi le catalogue de licences libres proposés</a>.</p>
			<p>Il appartient également à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…).</p>
			<p>Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents, il est lui-même considéré comme responsable des publications réalisées sur le service et doit organiser la modération à posteriori de ces modifications ou ajouts le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’Éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>

		<h3 id="securite"><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’Éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’Éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>

		<h3 id="limitation-de-responsabilite"><strong>8. Limitation de responsabilité</strong></h3>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’Éducation nationale.</p>
			<p>Le Ministère en charge de l’Éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’Éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>

		<h3 id="modification-des-cgu"><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’Éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 29 mars 2024.</p>

		<h3 id="duree-et-resiliation"><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’Éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’Éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’Éducation nationale et au profit de l’Utilisateur.</p>

		<h3 id="dispositions-diverses"><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>

	<h2 id="questionnaire"><strong>Conditions générales d&#39;utilisation - Outil de création de formulaire en ligne (Questionnaire)</strong></h2>

		<h3 id="objet"><strong>1. Objet</strong></h3>
			<p>Toute Personne physique, visiteur anonyme ou Utilisateur identifié pour accéder ou utiliser l'outil de création de formulaire en ligne ou à toute information publiée sur le Service, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.</p>
			<p>Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.</p>
			<p>Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.</p>
			<p>Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service et des contenus qui sont disponibles.</p>
		
		<h3 id="definitions"><strong>2. Définitions</strong></h3>
			<p>Condition Générale d’Utilisation (« CGU ») : désigne les présentes conditions d’utilisations du service.</p>
			<p>Éducation nationale : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).</p>
			<p>Service : désigne le service fourni par Ministère en charge de l’éducation nationale et ses sous-traitants, accessible par le biais du site ou du service.</p>
			<p>Site : désigne le site accessible depuis l’adresse <a href="https://questionnaire.apps.education.fr">https://questionnaire.apps.education.fr</a></p>
			<p>Utilisateur identifié : désigne toute personne physique qui répond à un questionnaire du Service, ou en ouvre un depuis son accès personnel dont l’accès est réservé à l’Utilisateur identifié, sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder fonctionnalités de création et d’administration.</p>		
			
		<h3 id="presentation-du-site"><strong>3. Présentation du site</strong></h3>
			<p>Questionnaire est un service permettant de concevoir des sondages / enquêtes simples, et de diffuser un lien vers des répondants pour collecter leurs réponses. C’est un service développé et hébergé au Ministère de l’Éducation nationale.</p>
			<ul>
				<p>Le service Questionnaire offre des fonctionnalités suivantes :</p>
				<ul>
					<li>création de formulaires à destination de répondants internes ou externes (grand public) ;</li>
					<li>intégration de différents types de questions ;</li>
					<li>création et intégration des mentions d’informations RGPD ;</li>
					<li>collectes de données, y compris données personnelles / sensibles ;</li>
					<li>présentation des réponses collectées sous forme de tableaux / graphiques ;</li>
					<li>export du fichier des réponses au format CSV ;</li>
					<li>suppression des données collectées automatiquement au bout de 90 jours après la clôture du questionnaire.</li>
				</ul>
			</ul>

		<h3 id="conditions-d-acces-generales"><strong>4. Conditions d'accès générales</strong></h3>
			<p>Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.</p>
			<p>Le Ministère en charge de l’éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.</p>
			<p>Le Ministère en charge de l’éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.</p>
			<p>Le Ministère en charge de l’éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.</p>

		<h3 id="regles-d-utilisation-du-service-et-engagement-de-l-utilisateur"><strong>5. Règles d'utilisation du service et engagement de l'utilisateur</strong></h3>
			<p>L’accès à la fonctionnalité de création de questionnaire est réservé aux personnes travaillant pour l’éducation nationale par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié) et en réponse seulement pour les autres.</p>
			<p>L’Utilisateur s’engage à ne pas :</p>
			<p>Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;</p>
			<ul>
				<p>Utiliser le site :</p>
				<ul>
					<li>pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;</li>
					<li>pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;</li>
					<li>de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;</li>
					<li>de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;</li>
					<li>de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ;</li>
					<li>de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;</li>
					<li>tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’éducation nationale, ou à tout autre utilisateur du Service ;</li>
					<li>supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.</li>
				</ul>
			</ul>
			<p>Le Ministère en charge de l’éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.</p>

		<h3 id="propriete-intellectuelle"><strong>6. Propriété intellectuelle</strong></h3>
			<p>À l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service Questionnaire sont protégés par la législation française sur le droit d'auteur et le droit des marques.</p>
			<p>L’ensemble des éléments du Service, les marques, logos, dessins, graphismes, chartes graphiques, icônes, textes, applications, scripts, fonctionnalité, ainsi que leur sélection ou combinaison apparaissant à l’adresse <a href="https://questionnaire.apps.education.fr">https://questionnaire.apps.education.fr</a>, sont la propriété exclusive du Ministère en charge de l’éducation nationale. Le code source du service peut néanmoins être retrouvé ici sous licence EUPL : <a href="https://gitlab.mim-libre.fr/alphabet/questionnaire">https://gitlab.mim-libre.fr/alphabet/questionnaire</a></p>
			<p>L’accès au Service n’entraîne aucune cession des droits susvisés.</p>
			<p>Les droits d’utilisation du Service ne sont concédés que sous forme numérique aux fins de visualisation des pages consultées, à titre personnel, non cessible et non exclusif.</p>
			<p>L’utilisateur s’interdit de copier, reproduire, modifier, distribuer, afficher ou vendre, par quelque procédé ou forme que ce soit, en tout ou partie, tout élément du site ou se rapportant à celui-ci, par quelque procédé que ce soit, et pour toute autre finalité y compris à titre commercial, sans l’autorisation préalable et écrite du Ministère en charge de l’éducation nationale.</p>
			<p>Les droits d’utilisation des contenus ajoutés sur le site par les Utilisateurs identifiés sont soumis à la licence Creative Commons Paternité - Partage à l’identique (CC By-SA). Il appartient à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…).</p>
			<p>Si l’Utilisateur identifié décide de faire participer des personnes externes aux agents du ministère sur le Service, il est lui même considéré comme responsable des publications qui y sont réalisées, et doit organiser lui même la modération à posteriori de ces participations le cas échéant.</p>
			<p>En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.</p>

		<h3 id="securite"><strong>7. Sécurité</strong></h3>
			<p>Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.</p>
			<p>Dans le cas où le Ministère en charge de l’éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.</p>

		<h3 id="limitation-de-responsabilite"><strong>8. Limitation de responsabilité</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable de la licéité, de l’exactitude et de la pertinence d’informations ou de contenus mis en en ligne ou modérés par l’Utilisateur identifié sous son entière responsabilité.</p>
			<p>L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.</p>
			<p>Le mot de passe est personnel et confidentiel.</p>
			<p>L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.</p>
			<p>Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’éducation nationale.</p>
			<p>Le Ministère en charge de l’éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.</p>
			<p>Le Ministère en charge de l’éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.</p>
			<p>L’Utilisateur est considéré, dans le cas de collecte de données à caractère personnel ou sensible, comme responsable de traitement. Il doit prendre toutes les dispositions nécessaires au traitement et à la protection des données ainsi collectées. Le Ministère en charge de l’éducation nationale ne saurait être tenu responsable des données collectées, et de l’information des répondants à ce sujet.</p>
		
		<h3 id="modification-des-cgu"><strong>9. Modification des CGU</strong></h3>
			<p>Le Ministère en charge de l’éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.</p>
			<p>Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.</p>
			<p>La version actuellement en vigueur est datée du 27 janvier 2025.</p>

		<h3 id="duree-et-resiliation"><strong>10. Durée et résiliation</strong></h3>
			<p>Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.</p>
			<p>Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’éducation nationale soit lorsque l’utilisateur se désinscrit.</p>
			<p>La résiliation correspond à la suppression du service par le Ministère en charge de l’éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’éducation nationale et au profit de l’Utilisateur.</p>

		<h3 id="dispositions-diverses"><strong>11. Dispositions diverses</strong></h3>
			<p>Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.</p>
			<p>Les présentes CGU sont soumises au droit administratif français.</p>
`;
