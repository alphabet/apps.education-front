export default {
  "academies": [
      {
        "id": "aix-marseille",
        "name": "Académie d'Aix-Marseille"
      },
      {
        "id": "amiens",
        "name": "Académie d'Amiens"
      },
      {
        "id": "besancon",
        "name": "Académie de Besançon"
      },
      {
        "id": "bordeaux",
        "name": "Académie de Bordeaux"
      },
      {
        "id": "clermont-ferrand",
        "name": "Académie de Clermont-Ferrand"
      },
      {
        "id": "corse",
        "name": "Académie de Corse"
      },
      {
        "id": "creteil",
        "name": "Académie de Créteil"
      },
      {
        "id": "dijon",
        "name": "Académie de Dijon"
      },
      {
        "id": "grenoble",
        "name": "Académie de Grenoble"
      },
      {
        "id": "outremer",
        "name":"Académie de Guadeloupe"
      },
      {
        "id": "outremer",
        "name":"Académie de Guyane"
      },
      {
        "id": "outremer",
        "name": "Académies d'Outre Mer"
      },
      {
        "id": "lille",
        "name": "Académie de Lille"
      },
      {
        "id": "limoges",
        "name": "Académie de Limoges"
      },
      {
        "id": "lyon",
        "name": "Académie de Lyon"
      },
      {
        "id": "outremer",
        "name":"Académie de Mayotte"
      },
      {
        "id": "outremer",
        "name":"Académie de Martinique"
      },
      {
        "id": "montpellier",
        "name": "Académie de Montpellier"
      },
      {
        "id": "nancy",
        "name": "Académie de Nancy"
      },
      {
        "id": "nantes",
        "name": "Académie de Nantes"
      },
      {
        "id": "nice",
        "name": "Académie de Nice"
      },
      {
        "id": "normandie",
        "name": "Académie de Normandie"
      },
      {
        "id": "outremer",
        "name":"Académie de Nouméa"
      },
      {
        "id": "orleans-tours",
        "name": "Académie d'Orléans-Tours"
      },
      {
        "id": "paris",
        "name": "Académie de Paris"
      },
      {
        "id": "outremer",
        "name":"Académie de Polynésie"
      },
      {
        "id": "poitiers",
        "name": "Académie de Poitiers"
      },
      {
        "id": "reims",
        "name": "Académie de Reims"
      },
      {
        "id": "rennes",
        "name": "Académie de Rennes"
      },
      {
        "id": "outremer",
        "name":"Académie de La Réunion"
      },
      {
        "id": "outremer",
        "name":"Académie de Saint-Pierre-et-Miquelon"
      },
      {
        "id": "strasbourg",
        "name": "Académie de Strasbourg"
      },
      {
        "id": "toulouse",
        "name": "Académie de Toulouse"
      },
      {
        "id": "versailles",
        "name": "Académie de Versailles"
      },
      {
        "id": "outremer",
        "name":"Académie de Wallis-et-Futuna"
      },
      {
        "id": "administration-centrale",
        "name": "Administration centrale"
      }
    ]
}
