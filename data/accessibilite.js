const declarationDeConformite = `
<h2>Déclaration d'accessibilité</h2>
<p>La direction du numérique pour l'éducation s’engage à rendre ses sites internet, intranet, extranet et ses progiciels accessibles conformément à l’article 47 de la loi n°2005-102 du 11 février 2005.</p>
<p>Cette déclaration d’accessibilité s’applique au site apps.education.fr et au bouquet de services numériques "Apps".</p>
<h3>Identité du déclarant</h3>
<p>Direction du numérique pour l'éducation</p>
<h3>Technologies utilisées pour la réalisation du site apps.education.fr</h3>
<ul>
  <li>HTML5</li>
  <li>CSS</li>
  <li>JavaScript (Nuxt.js)</li>
  <li>Images SVG et PNG</li>
</ul>
<h3>Agents utilisateurs, technologies d'assistance et outils utilisés pour vérifier l'accessibilité</h3>
<p>Les vérifications de restitution de contenus ont été réalisées sur les navigateurs et outils suivants: </p>
<p>Chrome 80</p>
<p>Firefox 75 / NVDA 2019.3.1</p>
<p>Les outils suivants ont été utilisés lors de l'évaluation :</p>
<ul>
  <li>WAVE</li>
  <li>WCAG Color Contrast Checker</li>
</ul>
<h3>Pages du site ayant fait l'objet de la vérification de conformité</h3>
<ul>
<li>Accueil</li>
<li>Description d'application</li>
<li>Bonnes pratiques</li>
<li>CGU</li>
<li>Mentions légales</li>
<li>Accessibilité</li>
</ul>
<h3>Résultats des tests</h3>
<p>Les résultats des tests de conformité seront communiqués ultérieurement.</p>
<h3>Critères non conformes restant à corriger</h3>
<p>La liste des critères non conformes restant à corriger sera communiquée ultérieurement.</p>`;

export default `
<h1>Accessibilité</h1>
<h2>Introduction</h2>

<h2>Qu’est-ce que l'accessibilité numérique ?</h2>
<p>L’accessibilité numérique permet d’accéder aux contenus (sites web, documents bureautiques, supports multimédias, intranets d’entreprise, applications mobiles…), quelle que soit sa façon de naviguer.Grâce à elle, tous les utilisateurs handicapés peuvent percevoir, comprendre, naviguer et interagir avec le Web.</p>
<p>Elle est essentielle aux personnes en situation de handicap, et bénéficie aussi aux personnes âgées dont les capacités changent avec l’âge. L’accessibilité numérique s’inscrit dans une démarche d’égalité et constitue un enjeu politique et social fondamental afin de garantir à tous, sans discrimination, le même accès à l’information et aux services en ligne.</p>
<p>L’accessibilité du Web repose sur 4 grands principes :</p>
<ul>
  <li>un site perceptible</li>
  <li>un site utilisable</li>
  <li>un site compréhensible</li>
  <li>un site compatible avec toutes les technologies d’assistance (lecteurs d’écran, loupes, claviers adaptés, etc.).</li>
</ul>


<h2>Politique d'accessibilité du site apps.education.fr</h2>
<p>La Direction du numérique pour l'éducation accorde un soin tout particulier à la qualité de réalisation et à l’amélioration progressive des sites web et applications concernés.
Elle est ainsi engagée dans une démarche visant à respecter le RGAA (Référentiel général d'amélioration de l'accessibilité), la Charte internet de l'Etat.</p>
<p>Afin d'obtenir le meilleur résultat possible, l'accessibilité est prise en compte par l'ensemble des équipes impliquées dans le développement du site et l'ensemble des contributeurs.</p>

${declarationDeConformite}

<h2>Droit à la compensation</h2>
<p>Il est important de rappeler qu’en vertu de l’article 11 de la loi de février 2005, la personne handicapée a droit à la compensation des conséquences de son handicap, quelles que soient l’origine et la nature de sa déficience, son âge ou son mode de vie.</p>
<p>Dans l’attente d’une mise en conformité totale, vous pouvez obtenir une version accessible des documents ou des informations qui y seraient contenues en envoyant un courriel à apps@education.gouv.fr en indiquant le nom du document concerné et/ou les informations que vous souhaiteriez obtenir.Les informations demandées vous seront transmises dans les plus bref délais.<p>

<h2>Signaler un problème d'accessibilité</h2>
</p>Vous pouvez nous aider à améliorer l'accessibilité du site en nous signalant les problèmes éventuels que vous rencontrez. Pour ce faire, envoyez-nous un courriel à apps@education.gouv.fr</p>

<h2>Défenseur des droits</h2>
<p>Si vous constatiez un défaut d’accessibilité vous empêchant d’accéder à un contenu ou une fonctionnalité du site, que vous nous le signaliez et que vous ne parveniez pas à obtenir une réponse rapide de notre part, vous êtes en droit de faire parvenir vos doléances ou demande de saisine à :</p>
<h3><strong>Via le site internet dédié:</strong></h3>
<a href="https://www.defenseurdesdroits.fr/contact">Formulaire de contact du site Web</a>
<h3><strong>Par courrier:</strong></h3>
<p>Le défenseur des droits</br>
7 rue Saint-Florentin 75409 Paris cedex 08</br>
<p>
<h3><strong>Par téléphone:</strong></h3>
<p>09 69 39 00 00 (coût d’une communication locale à partir d’un poste fixe)</p>`