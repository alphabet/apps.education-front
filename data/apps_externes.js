export default[
{
    id: "evento",
    title: "Evento",
    subtitle: "Planification d'événements",
    description: "",
    keyStat: "",
    icon: "evento",
    texts: [
        {
            title: "Planifiez vos événements en quelques clics!",
            body: `<p>Evento est un service d'aide qui vous permet de facilement planifier vos événements.</p> 
            <p> Le service Evento est développé par RENATER et est une personnalisation de l'application Moment. </p> 
            <p> Ce service est disponible à l'ensemble des établissements connectés à RENATER.
            Les personnes extérieures à la communauté peuvent participer aux Eventos, qu’elles soient à l’étranger ou en France, mais seuls les membres de la communauté peuvent créer des Eventos, en s’authentifiant au préalable via la fédération d’identité  </p>`
        }
    ]
},
{
    id: "filesender",
    title: "Filesender",
    subtitle: "Échange de fichiers",
    description: "",
    keyStat: "",
    icon: "filesender",
    texts: [
        {
            title: "Le transfert sécurisé de fichiers volumineux",
            body:`<p>FileSender vous permet de partager des fichiers pouvant atteindre jusqu’à 100Go tout en étant certifié que vos données sont sécurisées. Vos fichiers restent disponibles au téléchargement jusqu’à 30 jours. </p> 
            <p> Pourquoi l'utiliser: </p> 
            <ul>
            <li>À l’aide d’une invitation par mail ou d’un lien URL, tout utilisateur peut accéder aux documents, qu’il fasse partie de la communauté Éducation-Recherche ou non</li>
            <li>Vos interlocuteurs hors de la communauté Éducation-Recherche peuvent aussi vous envoyer des fichiers volumineux via une invitation.</li>
            <li> Vous pouvez suivre le nombre de téléchargements de vos documents par vos interlocuteurs en temps réel.</li>
          </ul>`
        }
    ]
},
{
    id: "france_transfert",
    title: "France Transfert",
    subtitle: "Transfert de fichiers",
    description: "",
    keyStat: "",
    icon: "france_transfert",
    texts: [
        {
            title: "Transfert de fichiers par des personnes externes",
            body:`<p>Un service interministériel d’envoi de fichiers volumineux, simple et sécurisé, entre agents de l’État, mais aussi avec les usagers, partenaires et prestataires.</p> 
            <p>Envoyez facilement des fichiers, en toute sécurité</p> 
            <ul>
            <li>Agent de l’État, vous souhaitez envoyer des fichiers lourds ?</li>
            <li>Un usager doit vous faire parvenir des pièces justificatives qui ne passent pas par courriel ?.</li>
            <li> Votre prestataire doit rapidement vous transmettre des documents volumineux?</li>
            </ul>
            <p> Développé et opéré par l’État, le service en ligne France transfert vous permet de partager des fichiers non sensibles très simplement entre agents et avec vos usagers et partenaires.</p>
            <p> Seul prérequis : le destinataire OU l’expéditeur doit être un agent de l’État. </p>`
        }
    ]
},
{
    id: "rendez_vous",
    title: "Rendez-vous",
    subtitle: "Échanger en audio et vidéo",
    description: "",
    keyStat: "",
    icon: "rendez_vous",
    texts: [
        {
            title: "Échanger en audio et vidéo en petit groupe",
            body:`<p>Ce service de webconférence mis à disposition du Ministère de l’Education nationale et de la Jeunesse par Renater, opérateur national du MENJS.</p> 
            <p>Il permet notamment :</p>
            <ul>
            <li>de créer simplement une webconférence en invitant des participants éloignés par l'envoi des informations de connexion.</li>
            <li>de communiquer et d'échanger en partageant des écrans, des présentations;</li>
            <li>d'échanger par messagerie instantanée lors de la conférence;</li>
            </ul>
            <p>Basé sur le logiciel libre Jitsi Meet, Jitsi peut également être utilisé via une application mobile.

            Ce service est compatible avec l'ensemble des navigateurs web récents quel que soit le terminal utilisé. Il favorise les interactions, les échanges et la mutualisation au sein d'un large public. </p>
            `
        }
    ]
},
{
    id: "tchap",
    title: "Tchap",
    subtitle: "Messagerie instantanée",
    description: "",
    keyStat: "",
    icon: "tchap",
    texts: [
        {
            title: "Messagerie instantanée et sécurisée des agents de l’Etat",
            body:`<p>Tchap est ouvert aux agents de la fonction publique, quel que soit leur statut, pour leur permettre de communiquer facilement et en toute sécurité.</p>
            <p>Plus en détail:</p>
            <ul>
            <li>Accédez à Tchap sur ordinateur et mobile.</li>
            <li>Trouvez facilement vos interlocuteurs de la fonction publique grâce à un annuaire intégré.</li>
            <li>Ne fournissez plus votre numéro de téléphone.</li>
            <li>Envoyez des messages, des fichiers, des photos et des messages vocaux.</li>
            <li>Invitez vos interlocuteurs externes à échanger sur vos salons privés</li>
            <li>Garantissez la confidentialité de vos messages privés grâce à un chiffrement de bout en bout. Même Tchap n’a pas accès aux messages des utilisateurs.</li>
            </ul>
            <p>Pour plus d'informations : https://tchap.beta.gouv.fr/?mtm_campaign=portail-apps-education-fr</p> `
        }
    ]
},
{
    id: "tribu",
    title: "Tribu",
    subtitle: "Espace collaboratif",
    description: "",
    keyStat: "",
    icon: "tribu",
    texts: [
        {
            title: "Service des espaces collaboratifs de l'Éducation nationale",
            body:`<p>Tribu est un service qui permet à un groupe d'utilisateurs de partager un espace collaboratif sécurisé dédié à un projet. Tribu permet à la communauté de partager des documents, des agendas, des taches, des forums de discussion. Il offre la possibilité d'être libre de son organisation et permet de travailler en ligne.</p> 
            `
        }
    ]
}
];