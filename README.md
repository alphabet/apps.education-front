# apps-education

> apps.education.fr

## Liens
* [Maquettes Figma](https://www.figma.com/file/9awGq6CtT54mUc1L153Gyl/WF---Page-apps-%2F-temporaire?node-id=0%3A1)

## Outils utilisés
* [Node 10.19](https://nodejs.org/en/)
* [Nuxt.js](https://nuxtjs.org)
* [Buefy](https://buefy.org/)
* [Material Design Icons](https://cdn.materialdesignicons.com/5.0.45/) + Icons persos

## Installation

```bash
# Installer les dépendences nécessaires au projet
$ yarn install

# Pour Lancer le serveur de développement sur http://localhost:8080
$ yarn dev

# Pour générer le site statique (génération de fichiers HTML)
$ yarn generate
```

## Données et Textes

### Données

Les données du site (Descriptifs des applications, bonnes pratiques) sont centralisées dans le dossier **data**

* **accessibilite.js** - Mentions concernant l'accessibilité du site (Déclaration de conformité [RGAA](https://www.numerique.gouv.fr/uploads/RGAA-v4.0.pdf] ))
* **applications.js** - Liste de toutes les applications avec des liens pour chaque acad + des descriptifs
* **bonnesPratiques.js** - Données pour les bonnes pratiques
* **cgu.js** - Textes en HTML pour les conditions générales d'utilisation
* **legal.js** - Textes de mentions légales et relatives aux traitements RGPD
* **platform.js** - Données liés à la plateforme (pour le moment les guichets pour chaque académie)

#### Exemple de HTML pour les descriptifs d'applications
``` js
{
  "texts": [
     { "title": "<strong>Rendez-vous</strong> permet de lancer une visio conférence",          
     "body": `
        <div>
           Une balise
        </div>
        <ul>
           <li>Sunt labore id laborum veniam magna minim enim nulla culpa duis laborum.</li>
           <li>Ut mollit exercitation irure consectetur officia in. Irure dolore sunt consequat cillum.</li>
        </ul>
        <a>Un lien</a>
     `
  ],
}

```

### Textes
* Les textes présents sur les différentes pages du site sont centralisés dans le fichier **locales/fr.json**

### Mesure d'audience (Matomo)
* Le tracking Matomo est en place dans le fichier **static/js/matomo.js**. Il est seulement fonctionnel en production et en passant la variable d'environnement TRACKING à true (via un fichier .env.production ou en spécifiant la variable au lancement du pipeline)