import { 
  applications,
  apps_externes,
  cgu, 
  accessibilite, 
  legal, 
  donneesPersonnelles,
  platform
} from '../data';

export default {
  findApplicationById(applicationId) {
    return new Promise((resolve) => {
      return resolve(applications.find((application) => application.id === applicationId));
    })
  },
  findApp_externeById(app_externeId) {
    return new Promise((resolve) => {
      return resolve(apps_externes.find((app_externe) => app_externe.id === app_externeId))
    })
  },
  findAllApplications() {
    return new Promise((resolve) => {
      resolve(applications)
    });
  },
  findAllApplications_externes() {
    return new Promise((resolve) => {
      resolve(apps_externes)
    });
  },
  cgu() {
    return new Promise((resolve) => {
      return resolve(cgu);
    })
  },
  accessibilite() {
    return new Promise((resolve) => {
      return resolve(accessibilite);
    })
  },
  legal() {
    return new Promise((resolve) => {
      return resolve(legal);
    })
  },
  donneesPersonnelles() {
    return new Promise((resolve) => {
      return resolve(donneesPersonnelles);
    })
  },
  findAllAcademies() {
    return new Promise((resolve) => {
      return resolve(platform.academies)
    })
  }
}
