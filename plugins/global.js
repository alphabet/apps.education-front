import Vue from 'vue';
import Container from '@/components/base/Container';
import Card from '@/components/base/Card';
import Message from '@/components/base/Message';
import Btn from '@/components/base/Btn';
import BackBtn from '@/components/base/BackBtn';
import TextField from '@/components/base/TextField';
import Icon from '@/components/base/Icon';
import Modal from '@/components/base/Modal';
import Autocomplete from '@/components/base/Autocomplete';
import Hero from '@/components/base/Hero';
import PlatformBtn from '@/components/platform/PlatformBtn';

Vue.component('Autocomplete', Autocomplete);
Vue.component('Modal', Modal);
Vue.component('Icon', Icon);
Vue.component('Btn', Btn);
Vue.component('BackBtn', BackBtn);
Vue.component('TextField', TextField);
Vue.component('Container', Container);
Vue.component('Card', Card)
Vue.component('Message', Message);
Vue.component('Hero', Hero);
Vue.component('PlatformBtn', PlatformBtn);

