FROM hub.eole.education/proxyhub/library/node:12-alpine AS build

ARG NODE_ENV=production

RUN mkdir /src
COPY . /src
WORKDIR /src

RUN yarn install
RUN yarn generate

FROM hub.eole.education/proxyhub/library/nginx:alpine

COPY --from=build  /src/public /usr/share/nginx/html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
