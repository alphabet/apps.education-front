# Changelog

## [2.3.0](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.2.0...release/2.3.0) (2025-02-17)


### Features

* **cgu:** add questionnaire cgu in cgu page ([0764a8b](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/0764a8b4a2c3225ad4f3940856f84e221cb98635))


### Bug Fixes

* **navbar:** add scroll margin because navbar hiding text ([b051252](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/b0512521a2fbc9d5cc079a902e85f77eb5aff740))

## [2.2.0](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.1.2...release/2.2.0) (2024-10-14)


### Features

* **legals:** change cgu, legals and personal data text ([acb9a10](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/acb9a101c2e94dd511a9da2de1068b4421bb59c7))

### [2.1.2](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.1.1...release/2.1.2) (2023-08-24)


### Styles

* corrige faute d'ortographe nextcloud ([48bae93](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/48bae93d7b348e4e1cf28dd9336448728650376d))

### [2.1.1](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.1.0...release/2.1.1) (2023-07-04)


### Bug Fixes

* pointe les liens de peertube et codimd vers ses texts ([84ff9e2](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/84ff9e29cfb801bf2ce2249701c38b0b9a5e290f))


### Styles

* changement des texts a propos et le projet apps.education.fr ([ca78705](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/ca7870589e94382c25c5b414dafe4fbef22ee6a8))
* corrige faute d'orthographe ([27eb6d8](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/27eb6d8d5a605eb80c91598d6ea4b725333b3315))
* corriger site mentions legales ([6b4bca8](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/6b4bca8e1bd95dce81ef47d1d3a40f64c8467894))
* **legal.js:** harmonisation du nom du PCLL ([3028d06](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/3028d068ca43d884f9dd72ae283f07e21549ad7f))
* suppression toute entree statut ([e4bf10a](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/e4bf10a8b5ca8a512e202468f2d712e37823ca95))

## [2.1.0](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.0.7...release/2.1.0) (2023-06-23)


### Features

* ajout de visio-agents dans les applications proposees ([d39f51a](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/d39f51a6951c92e9a1faeefd7507d0d39adda6ea))
* ajout des apps externes proposees ([1977c7b](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/1977c7ba50fb3857865896518c67d3965a5f2fdb))
* ajout des apps externes proposees ([0161f65](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/0161f657b81f0ca4e38e2a979caf842b1b2e3175))
* ajout des apps externes proposees ([c73b96d](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/c73b96d85578a3443faa3ab7040d71c68b5aec6f))
* ajout des apps externes proposees ([4de7817](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/4de781709714d985638ada3069de0beb0e0d1350))
* ajout des apps externes proposees ([37e6c00](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/37e6c00940b46c2b02b2766a8f206bbb2032f001))


### Bug Fixes

* ajout apps_externes dans index ([55f36ce](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/55f36ce2d6e35aa0b69c3edf2f3bee81ad6f1ac5))
* **content:** suppression de toutes les mentions de bonnes pratiques ([3d78154](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/3d78154b16d76c9c43486f3db9c99622650d9bd6))
* merge branch 'dev' into '23-remonter-le-projet ([ba53c08](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/ba53c082906a984ebc5e6f566bd7dca59eac6c75))


### Styles

* **content:** change la place du texte qui explique le projet apps ([329940e](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/329940e1a99004dcb246f81759881944485de88f))
* **content:** suppression du bouton vert a côté des applications ([4e73583](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/4e7358303f12b391ad092ecb6b437401d8338347))
* **content:** supression du label disponible dans les details de app ([6e226a3](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/6e226a33c2329b45ae94c64241d6952157967522))
* modification du link tchap qui etait trop longue ([28cca20](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/28cca20de76876e4676da912a834cb32e7e3481d))
* suppression bouton vert pour apps extrenes ([c5cde0e](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/c5cde0eee1ca82fd7e611678c9b99a2955151074))

### [2.0.7](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.0.6...release/2.0.7) (2023-05-24)


### Bug Fixes

* **content:** supprimer "Bonnes Pratiques" de l'index ([36274d3](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/36274d34ed6d00d2fd5d7f7c9689587279130d76))

### [2.0.6](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.0.5...release/2.0.6) (2022-09-15)


### Bug Fixes

* **content:** remove wording ([36e9b38](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/36e9b38a728ba409d1b06aae9dcd726262e15bb3))

### [2.0.5](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.0.4...release/2.0.5) (2022-08-19)


### Bug Fixes

* **content:** replace etherpad references by codiMD ([c3173e0](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/c3173e0d4d5b8eefc63330edce5025a28e18d398))

### [2.0.4](https://gitlab.mim-libre.fr/alphabet/apps.education-front/compare/release/2.0.3...release/2.0.4) (2022-08-19)


### Bug Fixes

* **doc:** modify text in classe virtuelle ([f30bc6b](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/f30bc6b098ea0177c136c9f378f013f160bab37f))
* **front:** delete acces to apps beta ([69fc4dd](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/69fc4dd038d0e56fa490d17bed079383c60a9d3b))
* **front:** remove eole mail ([4ddcf52](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/4ddcf528446576e251a8db04f3df073438aabc0f))
* **front:** remove pad ([f2934c5](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/f2934c59bfcbc839021a9b50c53c7dfc2e1a074f))


### Build System

* **ci:** maj file ci ([88505f8](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/88505f833d474fd132a29e0c65f3e988990cb99b))


### Code Refactoring

* **applications:** Replace etherpad application by classeVirtuelle application ([103b77f](https://gitlab.mim-libre.fr/alphabet/apps.education-front/commit/103b77fb603860d19ad2fb7b53204657d2f29710))
